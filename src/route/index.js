import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import {Home, Detail, Info, Provinsi} from '../pages';

const Menu = createStackNavigator();
const TabMenu = createBottomTabNavigator();

const TabScreen = () => {
  return (
    <TabMenu.Navigator>
      <TabMenu.Screen name="Home" component={Home} />
      <TabMenu.Screen name="Info" component={Info} />
      <TabMenu.Screen name="Provinsi" component={Provinsi} />
    </TabMenu.Navigator>
  );
};

const Route = () => {
  return (
    <Menu.Navigator>
      <Menu.Screen
        name="Home"
        component={TabScreen}
        options={{headerShown: false}}
      />
      <Menu.Screen name="Detail" component={Detail} />
    </Menu.Navigator>
  );
};

export default Route;
