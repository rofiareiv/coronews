import React, {Component} from 'react';
import {Text, StyleSheet, View} from 'react-native';
import api from '../../configs/api';

export default class Info extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    this.getData();
  }

  getData = () => {
    api.get('indonesia').then(hasil => {
      // console.log(hasil);
      this.setState({
        data: hasil.data,
      });
    });
  };

  render() {
    const {data} = this.state;
    return (
      <View>
        {data.map((item, key) => {
          return (
            <View key={key}>
              <Text> Info Corona {item.name} </Text>
              <Text> Sembuh {item.sembuh} </Text>
              <Text> Positif {item.positif} </Text>
              <Text> Meninggal {item.meninggal} </Text>
            </View>
          );
        })}

        <Text>nyoba</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
