import React, {Component} from 'react';
import {Text, View, FlatList, TouchableOpacity, StyleSheet} from 'react-native';
import api from '../../configs/api';

export class Provinsi extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dataProv: {},
    };
  }

  componentDidMount() {
    this.getData();
  }

  getData = () => {
    api.get('indonesia/provinsi').then(res => {
      // console.log(res);
      this.setState({
        dataProv: res.data,
      });
    });
  };

  rowData = ({item}) => {
    return (
      <TouchableOpacity
        style={styles.btn}
        onPress={() => this.props.navigation.navigate('Detail', {data: item})}>
        <Text>{item.attributes.Provinsi}</Text>
      </TouchableOpacity>
    );
  };

  render() {
    const {dataProv} = this.state;
    return (
      <View>
        <Text> Provinsi </Text>

        <FlatList
          data={dataProv}
          renderItem={this.rowData}
          keyExtractor={item => item.attributes.FID}
        />
      </View>
    );
  }
}

export default Provinsi;

const styles = StyleSheet.create({
  btn: {
    margin: 5,
    padding: 10,
    backgroundColor: 'lightgray',
  },
});
