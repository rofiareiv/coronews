import Home from './Home';
import Detail from './Detail';
import Info from './Info';
import Provinsi from './Provinsi';

export {Home, Detail, Info, Provinsi};
