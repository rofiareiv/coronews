import React, {Component} from 'react';
import {Text, View} from 'react-native';
import Intl from 'react-native-intl';

export class Detail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      Provinsi: '',
      positif: null,
      sembuh: null,
      mati: null,
    };
  }

  componentDidMount() {
    this.getData();
  }

  getData = () => {
    const {route} = this.props;
    const {data} = route.params;
    // console.log(data.attributes.FID);
    this.setState({
      Provinsi: data.attributes.Provinsi,
      positif: data.attributes.Kasus_Posi,
      sembuh: data.attributes.Kasus_Semb,
      mati: data.attributes.Kasus_Meni,
    });
  };

  formatNumber = value => {
    new Intl.NumberFormat(['ban', 'id']).format(value);
  };

  render() {
    const {Provinsi, positif, sembuh, mati} = this.state;
    return (
      <View>
        <Text> Provinsi {Provinsi}</Text>
        <Text>
          Jumlah Positif
          {positif}
        </Text>
        <Text> Jumlah Sembuh {sembuh} </Text>
        <Text> Jumlah Meninggal {mati} </Text>
      </View>
    );
  }
}

export default Detail;
