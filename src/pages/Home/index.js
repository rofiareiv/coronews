import React, {Component} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import api from '../../configs/api';

export class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      mhs: {
        nama: 'yudi',
        alamat: 'pademawu',
      },
    };
  }

  componentDidMount() {
    this.getData();
  }

  getData = () => {
    api.get('indonesia/provinsi').then(hasil => {
      // console.log(hasil);
      this.setState({
        data: hasil.data,
      });
    });
  };

  render() {
    const {data, mhs} = this.state;
    return (
      <View>
        <Text> Halaman Home </Text>
        {data.map((item, idx) => {
          return (
            <TouchableOpacity key={idx}>
              <Text>{item.attributes.Provinsi}</Text>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  }
}

export default Home;
